import spock.lang.Specification

import groovyx.net.http.*
import spock.lang.Shared


class WeatherTestSpec extends Specification {
	@Shared def testUrl, testRequest, testResponse
	def setupSpec(){
		testUrl = "http://api.openweathermap.org"
		testRequest = ['APPID': "b2ce5b9466a4cdcec5e7a6bf11465c5a"]
		testResponse = ''
	}

	def 'Check if city name and coordinates is returned correctly'() {

		when: 'Sent request to openweathermap'
		def http = new HTTPBuilder(testUrl)
		testRequest.put('q', cityReq)
		testResponse = http.get(path : '/data/2.5/weather', query : testRequest)
		println testRequest
		println testResponse

		then: "Check that 200 response code is returned"
		testResponse.cod == 200

		and: "Server returns correct city name"
		testResponse.name == cityResp

		where:
		cityReq||cityResp
		"Kharkiv,UA"||"Kharkiv"
		"London,GB"||"London"
	}
}