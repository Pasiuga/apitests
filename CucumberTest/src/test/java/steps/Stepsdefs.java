package steps;


import groovyx.net.http.HTTPBuilder;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import static runner.RunnerTest.testRequest;
import static runner.RunnerTest.testUrl;


public class Stepsdefs {

    @When("Sent request to openweathermap:")
    public void sent_request_to_openweathermap(DataTable dataTable){
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        HTTPBuilder http = null;
        try {
            http = new HTTPBuilder(testUrl);
            List<String> actualCity = new ArrayList<String>();
            actualCity = dataTable.asList(String.class);
            testRequest.put("q", actualCity);
            HashMap<String, Object> request = new HashMap<>();
            request.put("path", "/data/2.5/weather");
            request.put("query", testRequest);
            Object testResponse = http.get(request);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    void Then("Check that {int} response code is returned", (Integer int1) -> {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java8.PendingException();
    });

    void Then("Server returns correct city name and coordinates:", (DataTable dataTable) -> {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java8.PendingException();
    });
}
