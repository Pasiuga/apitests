package runner;

import io.cucumber.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;

@RunWith(Cucumber.class)


public class RunnerTest {

    public static String testUrl;
    public static Map<String, String> testRequest = new HashMap<>();

    @BeforeClass
    public static void setUp() throws Exception{
        testUrl = "http://api.openweathermap.org";
        testRequest.put("APPID", "b2ce5b9466a4cdcec5e7a6bf11465c5a");

    }
}
