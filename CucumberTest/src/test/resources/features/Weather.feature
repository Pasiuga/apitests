Feature: Test openweathermap

Scenario: Check if city name is returned correctly
      When Sent request to openweathermap:
      |"Kharkiv,UA"|
      |"London,GB"|
      Then Check that 200 response code is returned
      And Server returns correct city name:
      |"Kharkiv"|
      |"London"|